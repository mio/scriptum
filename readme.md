# scriptum

A tiny repository of simple Nim scripts.


## Contents

- nd2md: extract Nim doc notes and a list of types and procs from a Nim file to
  Commonmark Markdown.


## License

[0BSD](https://spdx.org/licenses/0BSD.html)
