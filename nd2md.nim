## =====
## nd2md
## =====
##
## :Author: mio
##
##
## A basic script to extract Nim documentation notes in a .nim module to
## a Commonmark Markdown file.
##
## Usage: nd2md in.nim out.md

import std/[base64, os, parseopt, strutils]


const app = (name: "nd2md", version: "0.1.0")


proc loadFile(file: string): string =
  ## Reads a text file and returns the contents as a string.
  try: return readFile(unixToNativePath(file))
  except IOError, OSError:
    debugEcho "Error: file cannot be read."
    return ""


proc saveFile(file: string, s: string, mode: FileMode = fmWrite) =
  ## Writes a string to file.
  try:
    var fh: File
    createDir(parentDir(unixToNativePath(file)))
    if open(fh, unixToNativePath(file), mode):
      write(fh, s)
      close(fh)
  except IOError, OSError, CatchableError:
    debugEcho "Error: file cannot be saved."


proc genAnchor(str: string): string =
  ## Given a line of an exported type or function declaration, returns a
  ## section link anchor with the type or function name and a truncated hash of
  ## the string.
  let
    name =
      if len(str) > 4:
        if split(str)[1] == "": split(split(strip(str))[0], "*")[0]
        else: split(split(str)[1], "*")[0]
      else: ""
    hash =
      if len(str) > 10: "-" & encode(str)[^10..^3]
      else: "-" & encode(str)
  "#" & name & hash


proc parse(str: string): string =
  ## Parses a nim file and extracts comments and function definitions.
  var
    extract, line = ""
    codeEnded, funcDefEnded, funcEnded, importEnded, mlEnded, typeEnded,
      typeSkipped = true
    splitStr = split(str, "\n")

  for n in 0..(len(splitStr) - 1):
    line = splitStr[n]

    # Metadata.
    if startsWith(line, "## :") and count(line, ":") == 2:
      extract = extract & "\n**" & split(line, ":")[1] & "**:" &
        split(line, ":")[2]

    # Code blocks inside comments.
    elif startsWith(line, "## .. code-block::"):
      extract = extract & "\n" & replace(replace(line, "## .. code-block::",
        "```"), "``` ", "```")
      codeEnded = false
    # Indented code block line inside comments. Unindent to top level to
    # line up with the code fences.
    elif not codeEnded and startsWith(line, "##  "):
      # Only unindent one level if the previous line was a function def.
      if endsWith(splitStr[n - 1], "="):
        extract = extract & "\n" & join(split(line, "## ")[1..^1], "## ")[2..^1]
      else:
        extract = extract & "\n" & strip(join(split(line, "## ")[1..^1], "## "))
    # Empty line within comments.
    elif not codeEnded and line == "##" and  n < (len(splitStr) - 1):
      # If next line does not resume indentation, end code block.
      if not startsWith(splitStr[n + 1], "##  "):
        extract = extract & "\n```\n"
        codeEnded = true
      else:
        extract = extract & "\n" & strip(join(split(line, "## ")[1..^1], "## "))
    # Last line in the file that may be the end of a code block.
    elif (codeEnded and line == "##" and n == len(splitStr)) or
      (not codeEnded and line == ""):
      extract = extract & "\n```\n"
      codeEnded = true

    # Function definitions.
    elif startsWith(line, "func") or startsWith(line, "proc") or
      startsWith(line, "method") or startsWith(line, "iterator") or
      startsWith(line, "macro"):
      # Ignore internal functions.
      if contains(line, "*("): funcEnded = false
      else: continue
      # Turn export asterisk into bold formatting in Markdown and end with
      # double space for newline. The description follows immediately in the
      # next line.
      let
        name = "<a name=\"" & genAnchor(line)[1..^1] & "\"></a>"
        boldRepl = @[("func ", name & "func **["), ("proc ", name & "proc **["),
          ("method", name & "method **["), ("iterator", name & "iterator **["),
          ("macro", name & "macro **[")]
      extract = extract & "\n\n" & replace(multiReplace(strip(line, false, true,
        {' ', '='}),  boldRepl), "*(", "](" & genAnchor(line) & ")**(") & "  "
      if not endsWith(line, "="): funcDefEnded = false
    # Multiline function definitions/signatures.
    elif not funcDefEnded:
      extract = extract & "\n" & strip(line, false, true, {' ', '=', '\n'}) &
        "  "
      if endsWith(line, "="): funcDefEnded = true
    # Function description.
    elif not funcEnded and startsWith(line, "  ##"):
      if strip(replace(line, "  ##")) == "": extract = extract & "\n"
      else: extract = extract & "\n" & replace(line, "  ## ", "")
    # Ignore empty lines inside functions.
    elif not funcEnded and n < (len(splitStr) - 1) and strip(line) == "":
      if startsWith(splitStr[n + 1], "  "): continue
      else: funcEnded = true

    # Ignore imports.
    elif startsWith(line, "import"): importEnded = false
    elif not importEnded and startsWith(line, "  "): continue
    elif not importEnded and line == "": importEnded = true

    # Regular comments and headings.
    elif startsWith(line, "## "):
      if n != len(splitStr) - 1:
        # L1 heading.
        if n == 1 and contains(splitStr[n - 1], "=") and
          strip(replace(splitStr[n - 1], "## "), chars = {'='}) == "" and
          contains(splitStr[n + 1], "=") and
          strip(replace(splitStr[n + 1], "## "), chars = {'='}) == "":
          extract = extract & replace(line, "## ", "# ")
        # L2 headings.
        elif contains(splitStr[n + 1], "=") and
          strip(replace(splitStr[n + 1], "## "), chars = {'='}) == "":
          extract = extract & "\n\n\n" & line
        # L3 headings.
        elif contains(splitStr[n + 1], "-") and
        strip(replace(splitStr[n + 1], "## "), chars = {'-'}) == "":
          extract = extract & "\n\n#" & line
        # Ignore underline of headings.
        elif (contains(line, "=") or contains(line, "-")) and
          strip(replace(line, "## "), chars = {'='}) == "" or
          strip(replace(line, "## "), chars = {'-'}) == "":
          continue
        else:
          extract = extract & "\n" & join(split(line, "## ")[1..^1])
    # Empty lines in comments. Only add if the next line is not also empty.
    elif line == "##" and n < (len(splitStr) - 1):
      if strip(splitStr[n + 1]) != "##" and strip(splitStr[n + 1]) != "":
        extract = extract & "\n"

    # Type, const, let and var definitions. Handle multiline string variables.
    elif line == "type" or line == "const" or line == "let" or line == "var":
      # Convert the keyword to heading.
      let headRepl = @[("type", "## Types"), ("const", "## Consts"),
        ("let", "## Lets"), ("var", "## Vars")]
      typeEnded = false
      if contains(line, "\"\"\""):
        extract = extract & "\n\n" & replace(line, "\"\"\"", "```")
        mlEnded = false
      elif n > 4:
        let newlines =
          if splitStr[n - 3] != "": "\n\n\n"
          else: "\n\n"
        extract = extract & newlines & multiReplace(line, headRepl)
      elif n < 4: extract = extract & "\n" & multiReplace(line, headRepl)
    # Keyword is not in its own line.
    elif (startsWith(line, "type") or startsWith(line, "const") or
      startsWith(line, "let") or startsWith(line, "var")) and
      (contains(line, "* =") or contains(line, "*=")):
      if not typeEnded and n < (len(splitStr) - 1):
        extract = extract & "\n" & line
        if strip(splitStr[n + 1]) == "": typeEnded = true
      else:
        let headRepl = @[("type", "## Types\n\ntype"),
          ("const", "## Consts\n\nconst"),
          ("let", "## Lets\n\nlet"), ("var", "## Vars\n\nvar")]
        typeEnded = false
        if contains(line, "\"\"\""):
          extract = extract & "\n\n" & replace(line, "\"\"\"", "```")
          mlEnded = false
        else: extract = extract & "\n\n" & multiReplace(line, headRepl)
    elif not typeEnded and n < (len(splitStr) - 1) and
      startsWith(line, "  ") and not startsWith(strip(line), "#"):
      # Type names. Only show exported types.
      if (contains(line, "* =") or contains(line, "*=")) and not
        startsWith(line, "    "):
        typeSkipped = false
        if contains(line, "\"\"\""):
          extract = extract & "\n\n" & replace(line, "\"\"\"", "```")
          mlEnded = false
        # Turn export asterisk into bold formatting in Markdown, add anchor and
        # end with double space for newline.
        else:
          let name = "<a name=\"" & genAnchor(line)[1..^1] & "\"></a>"
          extract = extract & "\n\n" & "  " & name & "**[" & strip(replace(line,
            "*", "](" & genAnchor(line) & ")**")) & "  "
      # Keys/properties in each type.
      elif not typeSkipped and startsWith(line, "    ") and
        strip(splitStr[n + 1]) == "":
        # Remove export asterisk which is used in Markdown formatting.
        extract = extract & "\n" & replace(line, "*", "") & "  "
        typeSkipped = true
      elif not typeSkipped and startsWith(line, "    ") and
        strip(line) != "":
        extract = extract & "\n" & replace(line, "*", "") & "  "
    elif not typeEnded and (splitStr[n + 3] == "" or not
      startsWith(splitStr[n + 3], " ")) and mlEnded:
      typeEnded = true
    elif not mlEnded and contains(line, "\"\"\""):
      extract = extract & "\n" & replace(line, "\"\"\"", "```")
      mlEnded = true

  # Trim newlines to a maximum of 2 empty lines between text.
  replace(extract, "\n\n\n\n", "\n\n\n")


proc showHelp() =
  ## Shows usage information.
  echo "Usage: " & app.name & " in.nim out.md"


proc showVersion() =
  ## Shows version information.
  echo app.name & " " & app.version


proc main() =
  ## The main proc which reads command-line parameters and handles command
  ## options.
  var input, output: string
  for kind, k, v in getopt():
    case kind
    of cmdArgument:
      if input == "": input = k
      elif output == "": output = k
      else: break
    of cmdShortOption, cmdLongOption:
      case k
      of "h", "help":
        showHelp()
        return
      of "v", "version":
        showVersion()
        return
    of cmdEnd: discard
  if input != "" and output != "": saveFile(output, parse(loadFile(input)))
  else: showHelp()


main()
